extends KinematicBody2D

const GRAVITY = 100.0
const WALK_SPEED = 200
const JUMP_HEIGHT = 125
const AIR_SPEED = 100

onready var ground_ray = get_node("GroundRay")
onready var start_pos = get_node("/root/Level/StartPosition").position

var velocity = Vector2()

func _physics_process(delta):
    velocity.y += delta * GRAVITY

    if Input.is_action_pressed("player_left") and ground_ray.is_colliding():
        velocity.x = -WALK_SPEED
    elif Input.is_action_pressed("player_right") and ground_ray.is_colliding():
        velocity.x =  WALK_SPEED
    elif Input.is_action_pressed("player_left") and not ground_ray.is_colliding():
        velocity.x = -AIR_SPEED
    elif Input.is_action_pressed("player_right") and not ground_ray.is_colliding():
        velocity.x =  AIR_SPEED
    else:
        velocity.x = 0

    if Input.is_action_pressed("player_jump") and ground_ray.is_colliding():
        velocity.y += -JUMP_HEIGHT

    move_and_slide(velocity, Vector2(0, -1))
	
#Used in debugging
func _process(delta):
    $Label.text = str(ground_ray.is_colliding())


func _ready():
    position = start_pos

func _on_death():
    position = start_pos