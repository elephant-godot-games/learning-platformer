extends Area2D

signal Hit

func _on_death_zone_body_entered(body):
	emit_signal("Hit")
